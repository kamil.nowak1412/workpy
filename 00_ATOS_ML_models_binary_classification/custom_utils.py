# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 09:43:21 2019

@author: Kamil Nowak
"""

import re
import copy as cp
import pandas as pd
import numpy as np
import unidecode
import sys
import time
from contextlib import contextmanager
import joblib
from operator import itemgetter
from sklearn import preprocessing
from sklearn.impute import SimpleImputer
from sklearn import metrics
import matplotlib.pyplot as plt


def format_column_name(text):
    """
    Function applies lower case and removes accents from string.
    """
    text = text.lower()
    text = unidecode.unidecode(text)
    
    return text


def find_objects(df):
    """
    Returns list of object variables in data frame.
    """
    #objects=[]
    col_names = df.columns
    indexes = list(np.where(df.dtypes==np.object))[0]
    
    objects = [col_names[index] for index in indexes]
    
    return objects


def find_missing_values(df):
    """
    Function returns data frame with information about missing values at each columns.
    """
    null_check = pd.DataFrame({'null_occurrence':list(df.isna().any()),
                               'null_ratio':[np.round(x,2) for x in list(df.isna().sum()/len(df))],'type':df.dtypes},index=df.columns)
    
    return null_check


@contextmanager
def elapsed_time(func_name=''):
    """
    Function calculates time of computation.
    """
    start_time = time.time()
    yield
    end_time = time.time()
    elapsed_time = end_time - start_time
    
    print(f'{func_name} took: {elapsed_time}')
    

def data_imputation(df, strategy_num='mean', fill_value=None):
    """
    Function imputes missing values.
    """
    data = cp.deepcopy(df)
    col_names = list(data)
    
    for name in col_names:
        temp = data.loc[:,name]
        if np.logical_and(temp.dtype == np.object, temp.isna().any()):
            temp.fillna('NULL',inplace=True)
            data.loc[:,name] = SimpleImputer(strategy='constant', fill_value='NULL', 
                                             copy=False).fit_transform(np.array(temp.ravel()).reshape(-1,1))
        if np.logical_and(np.logical_or(temp.dtype == np.int64, temp.dtype==np.float64),temp.isna().any()):
            data.loc[:,name].fillna(np.nan,inplace=True)
            if strategy_num=='constant':
                data.loc[:,name] = SimpleImputer(strategy=strategy_num, fill_value=fill_value, 
                                             copy=False).fit_transform(np.array(temp.ravel()).reshape(-1,1))
            else:
                data.loc[:,name] = SimpleImputer(strategy=strategy_num, copy=False).fit_transform(np.array(temp.ravel()).reshape(-1,1))
    
    return data


def object_to_encode(df, value='NULL', to_ignore=[], verbose=False):
    """
    Function transforms categorical variables into numerical ones using LabelEncoder.
    """
    data = cp.deepcopy(df)
    to_transform = find_objects(data)
    
    for col in to_transform:
        if col not in to_ignore:
            temp = data.loc[:,col].fillna(value=value).astype(str)
            label_encoder = preprocessing.LabelEncoder()
            feature = label_encoder.fit_transform(temp)
            feature = feature.reshape(data.shape[0],1)
            data.loc[:,col] = feature
        
    if verbose:
        print('Variables transformed:\n')
        print(set(to_transform)-set(to_ignore))
        
    return data
    
    
def make_string_percent(number):
    """
    Function transforms numeric value into string percentage.
    """
    text = str(np.round(100*number,1))+'%'
    
    return text    
    
    
def get_performance(y_true, y_pred_proba, y_pred_class, model_label):
    """
    Function creates data frame with calculated performance metrics.
    """
    auc = np.round(metrics.roc_auc_score(y_true,y_pred_proba),4)    
    gini = make_string_percent(2*metrics.roc_auc_score(y_true,y_pred_proba)-1)
    accuracy = make_string_percent(metrics.accuracy_score(y_true,  y_pred_class))
    mean_squared_error = np.round(metrics.mean_squared_error(y_true,y_pred_proba),4)
    
    df = pd.DataFrame({'AUC': auc, 'gini_ceofficient': gini, 'accuracy' : accuracy, 'mean_squared_error':mean_squared_error},
                      index=[model_label])
    
    return df
  
    
def compare_performance(df, target, cols, labels):
    """
    Function creates data frame wit performances for set of predictions.
    """
    results = pd.DataFrame()
    for i in range(len(cols)):
        temp = get_performance(df[target].values, df[cols[i]+'_predict_proba'], df[cols[i]+'_class'], labels[i])
        results = pd.concat([results,temp])
    results.index = labels
    
    return results   
    
    
    
def calculate_roc_curve(y_true, y_pred_proba):
    """
    Function calculates FPR, TPR and GINI coefficient.
    """
    fpr, tpr, tresh = metrics.roc_curve(y_true, y_pred_proba)
    gini = make_string_percent(2*metrics.roc_auc_score(y_true, y_pred_proba)-1)
    
    return fpr, tpr, gini    
    

def compare_roc_curves_for_single_sample(df, target, preds, labels, save=False, loc=''):
    """
    Function compares ROC curves for different predictions on single sample.
    """
    plt.xkcd()
    plt.figure(figsize=(12,12))
    
    for i in range(len(preds)):
        fpr, tpr, gini = calculate_roc_curve(df[target].values, df[preds[i]])
        plt.plot(fpr, tpr, label=labels[i]+', Gini = '+gini)

    plt.plot((0,1),(0,1),'-',c='black')
    plt.title('Comparison of ROC curves for considered models',fontsize=18)
    plt.xlabel('False Positive Rate (FPR)',fontsize=14)
    plt.ylabel('True Positive Rate (TPR)',fontsize=14)
    plt.legend(loc=4)
    plt.show()

    if save:
        plt.savefig(loc)
        print(f'Figure saved in: {loc}')   


def compare_roc_curves_many_samples(targets, preds, labels, save=False, loc=''):
    """
    Function compares ROC curves for different predictions on declared samples.
    """    
    plt.xkcd()
    plt.figure(figsize=(12,12))
    
    for i in range(len(preds)):
        fpr, tpr, gini = calculate_roc_curve(targets[i], preds[i])
        plt.plot(fpr, tpr, label=labels[i]+', Gini = '+gini)
    
    plt.plot((0,1),(0,1),'-',c='black')
    plt.title('Comparison of ROC curves for considered models',fontsize=18)
    plt.xlabel('False Positive Rate (FPR)',fontsize=14)
    plt.ylabel('True Positive Rate (TPR)',fontsize=14)
    plt.legend(loc=4)
    plt.show()
    
    if save:
        plt.savefig(loc)
        print(f'Figure saved in: {loc}')
    
