Directories:

data_raw: directory for raw data.
data_input: directory for preprocessed data, data frames with results stored in pickle files.
models: directory for storing the models in pickle files.
figures: directory for saving the plots and results from analyses.

Files: 
00_Atos_case_study.ipynb - final jupyter notebook, which contains all steps of building the models including comments and conclusions.
custom_utils.py - python script with all custom function created for the purpose of this project.
stroke_detection_presentation.ppt - presentation


